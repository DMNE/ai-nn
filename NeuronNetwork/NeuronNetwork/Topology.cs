﻿using System.Collections.Generic;

namespace NeuronNetwork
{
    public class Topology
    {
        public int InputCount { get; }
        public List<int> HiddenLayers { get; }
        public int OutputCount { get; }
        
        public Topology(int inputCount, int outputCount, params int[] layers)
        {
            InputCount = inputCount;
            OutputCount = outputCount;
            HiddenLayers = new List<int>();
            HiddenLayers.AddRange(layers);
        }
    }
}
